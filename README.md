<div align="center">

# EPSI Rennes B3 2022-2023
## Mise en Situation Professionnelle Reconstituée.
## Documentation technique et benchmark
<i>MSPR BLOC E6.1 – Concevoir et tester des solutions applicatives</i>
<br>
<i>MSPR BLOC E6.2 – Administrer & Concevoir des Solutions d’Infrastructure</i>
</div>

- [X] Dubosq Noé
- [X] Proust Stephen
- [X] Gouriou Aurélien
- [X] Bricaud Olivier 

# Contexte :

Dans le cadre de notre __Bachelor 3 Concepteur Développeur d'Application Spécialité IA__,
nous sommes tenus de réaliser une série d'exercices particuliers et très formateurs : les Mises en Situation Professionnelle Reconstituée (MSPR).

Dans l'optique de nous professionnaliser au mieux tout en mettant en application l'ensemble des compétences techniques vues au cours de notre formation, 
l'exercice se déroule comme suit : 
- nous formons des groupes de travail
- nous recevons un cahier des charges
- nous organisons notre gestion de projet et nos différentes activités avec les conseils d'un intervenant

Notons que les différentes MSPR sont plus ou moins liées, autant techniquement qu'en termes de perspective d'évolution. Nous prendrons cet élément en compte lors de nos choix techniques.
Une étude de différentes solutions (un benchmark) est attendue dans les livrables imposés.

La rédaction de ce document s'appuie sur les informations des deux premiers cahiers des charges :

MSPR 1 : 
```text
BLOC E6.1 – Concevoir et tester des solutions applicatives
« Développement et déploiement d’une application I.A dans le respect du cahier des charges Client »
```
MSPR 2 : 
```text
BLOC E6.2 – Administrer & Concevoir des Solutions d’Infrastructure
« Administrer & sécuriser une solution d’Infrastructure à partir d’un cahier des charges »
```
*MSPR relatives au TITRE PROFESSIONNEL CONCEPTEUR, DÉVELOPPEUR D’APPLICATIONS (CDA) RNCP 31678*


# Démarche
Nous allons parcourir différents domaines techniques afin d'étudier des solutions applicables à la problématique posée dans le sujet MSPR.
Nous motiverons nous choix selon les critères suivants :
- Problématique MSPR : La solution est adaptée aux enjeux de la MSPR.
- Temps imparti : Nous ne pouvons pas passer 2 semaines à apprendre une techno.
- Marché de l'emploi : Nous pouvons découvrir ou approfondir une techno recherchée.
- Mission d'alternance : Nous pouvons monter en compétences sur nos technos d'entreprise.
- Sécurité : La mise en application de pratiques de cybersécurité est primordial et doit être compatible avec la solution choisie.
- Appétence : La solution nous plaît et nous souhaitons la découvrir/connaître plus en profondeur.


| Critère              |  Validation  | Remarques |
|----------------------|:------------:|-----------|
| Problématique MSPR   |      -       | -         |
| Temps imparti        |      -       | -         |
| Marché de l'emploi   |      -       | -         |
| Mission d'alternance |      -       | -         |
| Sécurité             |      -       | -         |
| Appétence            |      -       | -         |


# Cahiers des charges

Suite à la lecture des deux cahiers des charges, nous définissons un scénario :

#### Acteurs
- [ ] Alice un utilisateur normal
- [ ] Bob un utilisateur botaniste
- [ ] Charlie un administrateur
- [ ] David un utilisateur normal

#### Scénario
```text
Alice se connecte et souhaite demander de l'aide pour sa Tulipe
Alice sélectionne Tulipe dans la liste officielle (+ photo officielle)
Si Tulipe n'existe pas, elle peut faire une demande et Charlie fera la mise a jour
Alice remplit sa requête, elle souhaite faire garder sa Tulipe du 17 au 18, 1 rue ABC
Alice remplit sa requête, elle renseigne des photos personnelles de sa Tulipe
Alice valide sa requête
Bob peut renseigner des informations générales sur Tulipe
Bob peut renseigner des informations spécifiques sur la Tulipe d'Alice (photos personnelles)
David peut répondre a Alice, en consultant les conseil généraux et spécifiques
```

#### Nous ciblons les sujets qui feront l'objet d'un benchmark
- [ ] Outils et Logiciels
- [ ] Solution technique Back
- [ ] Solution technique Front
- [ ] Solution technique Authentification
- [ ] Solution technique Base de données
- [ ] Solution technique Test & Déploiement
- [ ] Solution technique IA

<br>
<br>
<div align="center">

# Benchmark Outils et logiciels
</div>

## Communication, gestion de projet

Le premier outil de communication mis en place dans l'équipe est Discord, car nous en sommes tous les quatre utilisateurs réguliers.
Cela nous permet de mettre en place rapidement notre environnement gestion de projet, notamment avec la création de différentes discussions afin de :

- Centraliser les documents importants (sujets officiels, cahier des charges, calendrier)
- Réaliser un brain storming
- Mettre en place un backlog de projet et des backlog individuels
- Sauvegarder des liens, de la doc
- Partager des bouts de code
- Ne dédier ce salon discord qu'à ce projet


Dans le cadre de cette MSPR nous cherchons à limiter le nombre d'outils utilisés pour nous concentrer sur la phase de développement du projet. 
Nous ne souhaitions pas utiliser Trello associé à des mails, ou encore mettre en place une solution Jira.
En complément de Git, Google drive et un disque dur pour la sauvegarde des rendus, Discord nous permet une gestion de projet et une communication rapide à mettre en place 
en nous fournissant les features dont nous avons besoin.

<ins>Choix définitif : Discord.</ins>

## Logiciel de maquettage

Pour la réalisation des maquettes requises pour les MSPR, nous avons différentes contraintes :
- Des formats tous compatibles
- Des outils gratuits
- Des outils connus (de nous)

Nous éliminons donc la suite Adobe et Wireframe.

Les diagrammes UML seront faits avec le logiciel StarUML. 
Certaines features d'export depuis StarUML nous semble intéressantes à étudier.

Nous choisissons Figma et Mermaid pour nos schémas d'architecture et de documentation, 
avec l'export en SVG pour le premier et l'intégration dans les README.md pour le second.

<ins>Choix définitif : StarUML.</ins>


## Code

Nous sommes utilisateurs quotidiens de Git, et il nous est impensable de monter un projet sans. 
Le dépôt distant choisi est gitlab, avec un groupe privé MSPR et différents projets dedans (schématiquement : back, front, doc).
La mise en place d'une pipeline gitlab-ci est prévue, nous y reviendrons dans la partie *benchmark Test et Déploiement*.

Nos IDE de références sont VSCode et la suite Jetbrains. Pour ce projet, nous choisissons Pycharm et Webstorm.

<ins>Choix définitif : JetBrains.</ins>

<br>
<br>
<div align="center">

# Benchmark back end
</div>

## Extrait du cahier des charges

```text
[...] d’autant plus important que certaines des applications devront intégrer une intelligence artificielle afin de prédire des caractéristiques de la plante voulue.
L’application pourra communiquer avec une application web afin de soumettre des données et d’en récupérer.
```

## Langage

### Les technos rencontrées au cours de nos différents cursus :
- Python 
- Javascript
- Java
- Php

Nous sommes tous les 4 en spécialité IA et nous devons en intégrer une.

Nous devons implémenter un serveur back et une api d'accès aux données.

Deux membres du groupe font du python dans le cadre de leurs alternances.

| Critère              | Validation | Remarques                 |
|----------------------|:----------:|---------------------------|
| Problématique MSPR   |    oui     | -                         |
| Temps imparti        |    oui     | 3 à bien connaitre python |
| Marché de l'emploi   |    oui     | -                         |
| Mission d'alternance |    oui     | 2 sur 4 en entreprise     |
| Sécurité             |    oui     | -                         |
| Appétence            |    oui     | évidemment                |

<ins>Choix définitif : Python.</ins>

## Framework

Le choix unanime du Python comme langage de référence de la partie back end de notre projet MSPR écarte certains frameworks évoqués comme NodeJS (express), SpringBoot ou Symphony.

### Options de framework Python :

- Django
- FastAPI
- Flask

Malgré les avantages de légereté ou d'optimisation de performances de Flask ou FastApi, nous préférerons la robustesse de Django.

Django est un framework web complet, ouvert aux evolutions et adapté pour des projets mêlant base de données et fonctionnalités avancées.
Open source et suivant une architecture Modèle Vue Contrôleur, Django nous apporte sécurité, polyvalence et flexibilité afin de couvrir au mieux 
d'une part les attentes de l'exercice et d'autre part assurer une montée en compétences collective sur une techno qui a le vent en poupe. 
Nos critères de sélection sont respectés.

| Critere              | Validation | Remarques                     |
|----------------------|:----------:|-------------------------------|
| Problématique MSPR   |    oui     | Framework web complet         |
| Temps imparti        |    oui     | Connu par 2 membres du groupe |
| Marché de l'emploi   |    oui     | Plutôt orienté web            |
| Mission d'alternance |    oui     | 1 sur 4 mission d'entreprise  |
| Sécurité             |    oui     | -                             |
| Appétence            |    oui     | -                             |

<ins>Choix définitif : Django.</ins>

<br><br>
<div align="center">

# Benchmark Front end
</div>

## Extrait du cahier des charges

```text
L’application doit permettre à un utilisateur de faire garder ses plantes par un autre.
Pour cela elle donne la possibilité à cet utilisateur de prendre en photo les plantes qu’il souhaite faire garder.
...
Les besoins de l’application sont les suivants :
• Affichage des plantes à faire garder
• Ajout et visualisation de conseils concernant l’entretien d’une plante à faire garder.
```

## Langage et framework

À la lecture du cahier des charges, nous fixons nos cas d'utilisation et un scénario, et nous dégageons des fonctionnalités essentielles à la navigation et au parcours utilisateur :

- Gestion d'image
- Gestion de liste (de plantes, d'utilisateurs)
- Gestion d'article/de message avec réponse
- Gestion d'un compte utilisateur

À partir de ces conclusions, nous identifions différentes solutions adaptées à nos besoins :

- HTML/CSS/JS pur :
Cette solution couvre les besoins rudimentaires, mais risque d'être limitée en terme d'évolution et peut être fastidieuse à mettre en place.
Cependant, ces langages seront nécessaires pour certaines des solutions suivantes.

- Framework Web (Angular, React, Vue) :
Ces solutions offre déjà une architecture applicative et sont des technos porteuses sur le marché du travail actuel.

- Flutter
Le choix d'une application mobile est pertinente selon nos cas d'usage. 
Un utilisateur peut être sur son balcon ou dans son jardin, prendre en photo une de ses plantes et faire une demande de garde.
L'avantage de Flutter est sa polyvalence en cible de déploiement.
Son intégration avec une base de données SQLite nous offre une légèreté et une mobilité intéressante dans ce contexte d'application de garde de plante.

- TkInter
Si le client en émet le besoin, une petite interface TkInter pour les botanistes afin de superviser et monitorer la solution IA s'intégrera très bien avec notre back end python.

L'envie de développer une application complète, fonctionnelle et aboutie associée au besoin de monter en compétence 
sur un framework populaire nous encourage à choisir le framework Angular.
Nos critères de choix sont respectés, notamment avec l'implémentation de TypeScript qui permet d'éviter des problèmes courant avant la compilation. 
TypeScript permet également une programmation orientée objet avec des interfaces et des classes.

| Critère              | Validation | Remarques        |
|----------------------|:----------:|------------------|
| Problématique MSPR   |    oui     | -                |
| Temps imparti        |    non     | 1 membre connais |
| Marché de l'emploi   |    oui     | -                |
| Mission d'alternance |    oui     | -                |
| Sécurité             |    oui     | -                |
| Appétence            |    oui     | -                |

<ins>Choix définitif : Angular.</ins>

## Solution capture d'images :
```text
L’application devra permettre l’utilisation de l’appareil photo, un benchmark des outils et technologies est
important.
Elle permettra à l’utilisateur d’enregistrer ses plantes avec leurs localisations afin de pouvoir entrer en contact
avec un botaniste.

Les besoins de l’application sont les suivants :
• Affichage des plantes à faire garder
• Prise en photo de plantes et partage
• Ajout et visualisation de conseils concernant l’entretien d’une plante à faire garder.
```

La prise de photo sera faite depuis l'application Flutter, à l'aide du package CameraPlugin. Ces photos pourront être enregistrées depuis l'application mobile, et consultées depuis l'application et le site web Angular.

| Critère              | Validation | Remarques |
|----------------------|:----------:|-----------|
| Problématique MSPR   |    oui     | -         |
| Temps imparti        |    non     | -         |
| Marché de l'emploi   |     -      | -         |
| Mission d'alternance |    non     | -         |
| Sécurité             |    oui     | -         |
| Appétence            |     -      | -         |

<ins>Choix définitif : Flutter CameraPlugin.</ins>

<br><br>
<div align="center">

# Benchmark Sécurité et authentification
</div>

## Extrait du cahier des charges
```text
Le second lot comprend un premier aspect réglementaire et une sécurisation de l’accès aux données des
utilisateurs :
- Respect de la réglementation européenne
- Comptes utilisateurs avec des droits
- Authentification des utilisateurs
- Stockage de données sécurisé
```

## Respect du RGPD

Afin de respecter le RGPD coté backend, nous avons fait le choix de fractionner nos tables concernant les utilisateurs.
Les données enregistrées seront chiffrées.

![Schema base de données conforme RGPD en annexe](assets/rgpd_db.svg)


Pour la partie front end, nous penserons aux différents éléments requis :
- Finalité du traitement
- Droit de regard
- Contact du responsable de traitement
- Licéité
- Consentement éclairé (checkbox pas cochées)

## Solution d'authentification 

Afin de satisfaire les attentes de cette MSPR, nous devons envisager la gestion des accès à différents niveaux. 

Nous mettons en avant deux aspects de stratégie d'authentification :

- [ ] Coté applicatif

Quel user de quelle machine peut utiliser l'app, taper sur la db etc.
À l'heure actuelle, le choix n'est pas définitif et des conclusions du *benchmark solutions de déploiement* plus bas nous seront utiles (gestion des droits Docker..).

- Auth0
- Keycloak

En sa qualité de framework web complet, Django présente des sécurités contre les injections SQL, le Cross Site Scripting (XSS) ou le Cross Site Request Forgery (CSRF).

- [ ] Coté utilisateur

Nous devons garantir à un utilisateur qu'il peut se créer un compte avec un mot de passe secret, 
que nous protégeons son mot de passe et qu'il peut se connecter et accéder à ses données apres s'être connecté avec son nom d'utilisateur et son mot de passe.

En python, nous disposons des librairies *hashlib* (2010) et *Bcrypt* (2013). 
La première nous permet de centraliser différents algorithmes standards de hachage sécurisés. La seconde propose un processus robuste de hashage et dont l'attaque nécessite un matériel coûteux et puissant.

Django implémente également un certain nombre de solutions de sécurité telle que :
- une version de la fonction de dérivation de clé Argon2, gagnant de la Password Hashing Competition de 2015.
- Auth0, avec la mise en place 


<br><br>
<div align="center">

# Benchmark Base de données
</div>

## Extrait du cahier des charges

```text
Certaines données stockées sont sensibles et ne peuvent être stockées ainsi.
Il est important de prendre un moment pour les qualifier et de changer la façon de l’application de les stocker
et d’interagir avec.
Une veille sur les différentes architectures sécurisées est recommandée.
```

## Techno connues :

- Mysql (GNU GPL)
- Postgresql (Licence BSD)
- Mariadb
- Mongodb
- SQLite

Selon le cahier des charges et les cas utilisateurs envisagés, nous considérons dès à présent l'environnement mobile comme primordial. 
Ainsi le choix d'une premiere implémentation du projet avec une base SQLite se fait naturellement. Nous n'omettons pas la nécessité potentielle d'une base de données plus 'solide', comme une PostgresSQL qui fonctionne très bien avec Python.

```python
# settings.py de notre application Django
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
```

| Critère              | Validation | Remarques                                 |
|----------------------|:----------:|-------------------------------------------|
| Problématique MSPR   |    oui     | -                                         |
| Temps imparti        |    non     | -                                         |
| Marché de l'emploi   |    oui     | Très porteur avec le développement mobile |
| Mission d'alternance |    non     | -                                         |
| Sécurité             |    oui     | -                                         |
| Appétence            |    oui     | Très porteur avec le développement mobile |

<ins>Choix définitif : SQLite.</ins>

## Stockage, sauvegarde et recupération

- Technologie RAID
  - 0 tout est split sur différents disques, tres rapide, mais si 1 tombe c'est mort
  - 1 double copie, permet d'en perdre 1, mais pas rapide
  - 5 tout split dans X disques avec le bloc de parité
- Nas
- Cloud (+redondance, - secu)
- Solutions a étudier : SQLBackupAndFTP, iperius backup, N-able Cove Data Protection, Ottomatik.io

<br><br>
<div align="center">

# Benchmark solution de test et déploiement
</div>

```mermaid
graph LR
  subgraph DEPLOY 
    subgraph TDD
        IN[ ]-->fail --> code --> ok --> fail
    end
    
    subgraph CICD
        A[pipeline feature] --merge--> B[pipeline develop] --merge--> C[merge master] 
    end
    
    subgraph docker
        build --> publish --> OUT[ ]
    end
    
    ok -- makefile --> A
    C -- success --> build 

  end
```

<div align="center" style="font-size:.8em; margin-top: -15px">Schéma processus test et déploiement</div>

## Extrait du cahier des charges tests
```text
Afin de garantir ces fonctionnalités durant les évolutions de l’application, des tests de non-régressions sont attendus.
Ces tests peuvent être :
– Des tests unitaires
– Des tests d’intégrations
– Des tests système
– des tests d’acceptance
```

Considérant nos deux principales stacks techniques, back end en Python/Django et front end en Angular/Js, 
nous avons fait le choix d'utiliser respectivement les librairies de test suivantes : pytest/unittest et karma.


## Extrait du cahier des charges déploiement
```text
Les parties de l’application ne s’exécutant pas sur une machine de l’utilisateur (smartphone, ordinateur...) elles devront être
containerisées afin de simplifier le déploiement.
```

Considérant le marché actuel et nos différents modules de cours, deux options s'offrent à nous : Docker et Jenkins.
Ayant mis en place des pipelines de test et déploiement en entreprise, du test unitaire à la publication de l'image Docker, nous choisirons cette solution.
La technologie Docker offre une scalabilité et une gestion de version qui nous semble cohérente avec les enjeux de ce projet.

Notre projet se décompose que suit :

- Une branche master protégée.
- Une branche develop depuis laquelle nous travaillons.
- Des branches features/... pour nos différentes features.

Nous mettrons en place des tests unitaires pour les blocs logiciel algorithmiques (notamment pour la partie IA) ainsi que des tests fonctionnels pour pallier d'éventuelles regressions.
La démarche mise en place :
- Des commandes Make dans un Makefile qui démarrent nos tests.
- Ces mêmes commandes utilisées dans le gitlab-ci.yml. À chaque push sur une branche :
  - Une étape tests unitaires
  - On build l'image
  - Tests fonctionnels
  - Tests intégration

Si la pipeline est verte, alors on peut merge nos branches feature/... dans develop, puis dans master avec une nouvelle pipeline verte, ensuite on peut publier l'image et la deployer aisément.

| Critere              | Validation | Remarques                             |
|----------------------|:----------:|---------------------------------------|
| Problématique MSPR   |    oui     | Déploiement, scalabilité, flexibilité |
| Temps imparti        |    oui     | -                                     |
| Marché de l'emploi   |    oui     | -                                     |
| Mission d'alternance |    oui     | 1 membre                              |
| Sécurité             |    oui     | -                                     |
| Appétence            |    oui     | -                                     |

<ins>Choix définitif : Gitlab CICD & Docker</ins>


<br><br>
<div align="center">

# Benchmark IA
</div>

## Extrait du cahier des charges
```text
VI – COLLECTE DE DONNÉES ET MACHINE LEARNING 
(Pour les apprenants du parcours DEV I.A) 
Il est possible de collecter des données sur les plantes (photos, espèce, conseils d’entretiens) afin d’automatiser les conseils 
sur les plantes ou la reconnaissance des espèces de plantes.
Il est recommandé de faire une veille sur les banques de données existantes et sur les modélisations déjà effectuées avant 
de préparer le processus de collecte de données.
Les outils de collecte devront être choisi de telle façon à ce que la collecte soit industrialisable, les données collectées 
seront stockées dans une base SQLITE, ce qui permettra leur import dans l’application mobile.
Une fois un modèle de machine learning obtenu, il sera intégré dans l’application mobile. Un benchmark des technologies 
de déploiement/d’intégration est attendu
```

Nous avons tous les quatre fait le choix de suivre la spécialité *Intelligence Artificielle* au cours de notre Bachelor CDA DevOps, le cahier des charges nous annonce l'implémentation requise d'une solution IA.
Le choix de Python comme langage pour notre stack back end n'était pas anodin, ainsi nous avons accès à plusieurs outils :

- La librairie Scikit learning, qui fournit des modèles et permet de mettre en place des processus d'entrainement de ces modèles.
- Les librairies MatPlotlib et SeaBorn qui nous servirons à visualiser nos données.
- La puissance de numpy :^)
- La librairie Panda utile a la gestion de fichier csv comme source/dataset. 

En l'état la solution IA n'est pas une priorité, la problématique précise sera énoncée dans un cahier des charges de MSPR ultérieure.
En effet, l'enjeu et les démarches mises en places diffèrent s'il s'agit de reconnaissance sur une photo arbitraire, ou s'il s'agit par exemple de clustering de conditions d'entretien d'une plante.
La collecte, la normalisation et l'utilisation des données feront l'objet d'une rigueur particulière.

En attente :
- Discuter benchmark IA avec nos formateurs
- Savoir comment c'est évaluer
- Définir la problématique
- Attendre les sujets MSPR à venir

Technos:
- scikit (build on NumPy, SciPy, and matplotlib) => Machine learning
- openAI => API en ligne
- Pytorch => Machine learning

<br>

- Matplotlib => outil modélisation
- Seaborn => outil modélisation
- Pandas => manipulation de data
- cudf => GPU dataframe library

<br>

- numpy => calcul
- tensorflow => logiciel de calcul haute performance
- Dask => parallel computing
- PySpark => couche d'apache spark, analyse, data engineering
- Koalas => panda sur apache spark
- Rust ?

<ins>Choix : à définir.</ins>

<br><br>
<div align="center">

# Résultat des différents benchmarks
</div>

| Domaine              |       Choix        | Remarques |
|----------------------|:------------------:|-----------|
| Back                 |   Python Django    | -         |
| Front                | Javascript Angular | -         |
| Base de données      |       SQLite       | -         |
| Sécurité             |     Dango lib      | -         |
| Tests et déploiement |    Gitlab CICD     | -         |



